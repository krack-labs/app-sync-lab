# Lab App Sync

Lab pour présenter AppSync pour faire du Server Send Event depuis du serverless (AWS lambda)

## archi
```plantuml
@startuml component
!include <aws/common>
!include <aws/Storage/AmazonS3/AmazonS3>
!include <aws/Compute/AWSLambda/AWSLambda>
!include <aws/Compute/AWSLambda/LambdaFunction/LambdaFunction>
!include <aws/Database/AmazonDynamoDB/AmazonDynamoDB>
!include <aws/Database/AmazonDynamoDB/table/table>


!include <aws/common>
!include <aws/ApplicationServices/AmazonAPIGateway/AmazonAPIGateway>
!include <aws/Compute/AWSLambda/AWSLambda>
!include <aws/Compute/AWSLambda/LambdaFunction/LambdaFunction>
!include <aws/Database/AmazonDynamoDB/AmazonDynamoDB>
!include <aws/Database/AmazonDynamoDB/table/table>
!include <aws/General/AWScloud/AWScloud>
!include <aws/General/client/client>
!include <aws/General/user/user>
!include <aws/SDKs/JavaScript/JavaScript>
!include <aws/Storage/AmazonS3/AmazonS3>
!include <aws/Storage/AmazonS3/bucket/bucket>
!define AWSPuml https://raw.githubusercontent.com/awslabs/aws-icons-for-plantuml/v11.1/dist

!includeurl AWSPuml/AWSCommon.puml

!includeurl AWSPuml/SecurityIdentityCompliance/Cognito.puml



USER(user1, "Achille") 
USER(user2, "Bertille")


AWSCLOUD(aws) {



    AMAZONAPIGATEWAY(api)

    AWSLAMBDA(lambda) {
        LAMBDAFUNCTION(send,"send")

        LAMBDAFUNCTION(resend,"resend")
    }
}



user1 --> api : 1. Mutation Send(message): message

api --> send:  2. Function Send

send --> api : 3. Mutation Resend(message): event

api --> resend : 4. Function Resend

resend --> user2 : 5. Subscription subscribeToMessage(Event)
@enduml
```

## initialisation du projets
```bash
npm install aws-amplify
```

## création du service graphql

```bash
$ amplify add api
? Select from one of the below mentioned services: GraphQL
? Here is the GraphQL API that we will create. Select a setting to edit or continue Continue
? Choose a schema template: Blank Schema

```

`amplify/backend/function/resend/src/package.json`
```graphql

input AMPLIFY { globalAuthRule: AuthRule = { allow: public } } # FOR TESTING ONLY!

# useless but it's for fixing a bug.
type Query {
    message:[Message!]
}
type Mutation {

    send(message: MessageInput!): Message!
    @function(name: "send-${env}") 

    resend( message: MessageInput!): MessageEvent! 
    @function(name: "resend-${env}") 

}

type Subscription {
  subscribeToMessage: MessageEvent
  @aws_subscribe(mutations: ["resend"])
}

type Message {
    pseudo: String!
    message: String!
}
input MessageInput {
    pseudo: String!
    message: String!
}
type MessageEvent{
    id: ID!
    message: Message!
}
```


## création des fonctions
### Send

```bash
$ amplify add function
? Select which capability you want to add: Lambda function (serverless function)
? Provide an AWS Lambda function name: send
? Choose the runtime that you want to use: NodeJS
? Choose the function template that you want to use: Hello World

Available advanced settings:
- Resource access permissions
- Scheduled recurring invocation
- Lambda layers configuration
- Environment variables configuration
- Secret values configuration

? Do you want to configure advanced settings? Yes
? Do you want to access other resources in this project from your Lambda function? Yes
? Select the categories you want this function to have access to. api
? Select the operations you want to permit on appsynclab Mutation

You can access the following resource attributes as environment variables from your Lambda function
        API_APPSYNCLAB_GRAPHQLAPIENDPOINTOUTPUT
        API_APPSYNCLAB_GRAPHQLAPIIDOUTPUT
        API_APPSYNCLAB_GRAPHQLAPIKEYOUTPUT
        ENV
        REGION
? Do you want to invoke this function on a recurring schedule? No
? Do you want to enable Lambda layers for this function? No
? Do you want to configure environment variables for this function? No
? Do you want to configure secret values this function can access? No
```

`/amplify/backend/function/send/src/index.js`
```javascript

const fetch = require( 'node-fetch');
exports.handler = async (event) => {
    console.log(`EVENT: ${JSON.stringify(event)}`);
    await notify(event.arguments.message);
    return event.arguments.message;
};


async function notify(message){

    const query = /* GraphQL */ `
        mutation resend($message: MessageInput!) {
            resend(message: $message) {
                id
                message {
                    pseudo
                    message
                }
            }
        }
    `;
    const variables = {
        message: message
    };

    const options = {
        method: 'POST',
        headers: {
            'x-api-key': process.env.API_APPSYNCLAB_GRAPHQLAPIKEYOUTPUT
        },
        body: JSON.stringify({ query, variables })
    };

    var response = await fetch(process.env.API_APPSYNCLAB_GRAPHQLAPIENDPOINTOUTPUT, options);
    await response.json();
}
```
`amplify/backend/function/send/src/package.json`
```JSON
"dependencies": {
    "node-fetch": "^2.6.7"
},
```
### resend 

```bash
$ amplify add function
? Select which capability you want to add: Lambda function (serverless function)
? Provide an AWS Lambda function name: resend
? Choose the runtime that you want to use: NodeJS
? Choose the function template that you want to use: Hello World

Available advanced settings:
- Resource access permissions
- Scheduled recurring invocation
- Lambda layers configuration
- Environment variables configuration
- Secret values configuration

? Do you want to configure advanced settings? No
```

`amplify/backend/function/resend/src/index.js`
```javascript 
const { v4: uuidv4 } = require('uuid');
exports.handler = async (event) => {
    console.log(`EVENT: ${JSON.stringify(event)}`);
    return {
        id: uuidv4(),
        message:  event.arguments.message
    };
};

```

`amplify/backend/function/resend/src/package.json`
```JSON
"dependencies": {
    "uuid": "^8.3.2"
},
```

### envoyer les données sur les serveurs
```bash
amplify push -y
```

## ajout du formulaire
`index.html`
```HTML
<form>
    <div>
        <label for="pseudo">pseudo </label>
        <input id="pseudo" type="text" required/>
    </div>
    <div>
        <label for="message">message </label>
        <input id="message" type="text" required />
    </div>
    <input type="submit" value="OK" />
</form>

<div class="messages"></div>
```
Intégration de amplify
`src/app.js`
```javascript
import { Amplify, API, graphqlOperation } from '../node_modules/aws-amplify';
import awsconfig from './aws-exports';
import { send } from './graphql/mutations';

import * as subscriptions from './graphql/subscriptions';
import { v4 as uuidv4 } from 'uuid';
Amplify.configure(awsconfig);

```

action globales
`src/app.js`
```javascript
createUniquePseudo();
registerNewMessage();
registerSubmit();
```

Trop faignant pour écrire le pseudo moi même.
`src/app.js`
```javascript
function createUniquePseudo(){
    document.querySelector("#pseudo").value = "user-"+uuidv4();
}
```

on envoi le message lors submit du formulaire
`src/app.js`
```javascript
function registerSubmit(){
    
    document.querySelector("form").addEventListener("submit",  (event) =>{
        const message = { pseudo: document.querySelector("#pseudo").value, message: document.querySelector("#message").value };
        sendMessage(message);
        document.querySelector("#message").value = '';
        event.preventDefault();
        event.stopPropagation();
    }, false);
}

function sendMessage(message){
    API.graphql(graphqlOperation(send, {message: message}));
}
```
On ecoute le subscribe pour ajouter le message
`src/app.js`
```javascript

function registerNewMessage(){
 API.graphql(
    graphqlOperation(subscriptions.subscribeToMessage )
  ).subscribe({
    next: ({ provider, value }) =>{
        addNewMessage(value.data.subscribeToMessage.message);
    } ,
    error: error => console.warn(error)
  });
};

function addNewMessage(message){
    var messages = document.querySelector(".messages");
   
    var blockMessage = document.createElement("div");
    addPseudo(message.pseudo, blockMessage);
    addMessage(message.message, blockMessage);
    
    messages.append(blockMessage)
}

function addPseudo(pseudo, block){
    var pseudoBlock =  document.createElement("span");
    pseudoBlock.classList.add('pseudo');
    pseudoBlock.textContent=pseudo;
    block.append(pseudoBlock);
}
function addMessage(message, block){
    var messageBlock =  document.createElement("span");
    messageBlock.classList.add('message');
    messageBlock.textContent=message;
    block.append(messageBlock);
}

```
